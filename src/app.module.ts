import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CatEntity } from './db/entities/cat.entity';
import { UserEntity } from './db/entities/user.entity';
import { CatController } from './module/cat/cat.controller';
import { CatService } from './module/cat/cat.service';
import { UserController } from './module/user/user.controller';
import { UserService } from './module/user/user.service';

const DB_PATH = __dirname + '/db/data.db';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: DB_PATH,
      entities: [CatEntity, UserEntity],
      synchronize: true,
    }),
  ],
  controllers: [UserController, CatController],
  providers: [UserService, CatService],
})
export class AppModule {}
