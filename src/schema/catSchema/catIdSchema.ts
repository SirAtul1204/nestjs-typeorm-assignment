const Joi = require('joi');

export const catIdSchema = Joi.object({
  id: Joi.number().required(),
});
