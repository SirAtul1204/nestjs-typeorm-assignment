import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  UsePipes,
} from '@nestjs/common';
import { JoiValidationPipe } from 'src/JoiValidationPipe';
import { CatService } from './cat.service';
import { CreateCatDto } from './createCatDto';
import { createCatSchema } from '../../schema/catSchema/createCatSchema';
import { catIdSchema } from '../../schema/catSchema/catIdSchema';
import { catAgeRangeSchema } from 'src/schema/catSchema/catAgeRangeSchema';
import { updateCatSchema } from 'src/schema/catSchema/updateCatSchema';

@Controller()
export class CatController {
  constructor(private readonly catService: CatService) {}

  @Get()
  async getCat() {
    const cats = await this.catService.getCat();
    return cats.length ? cats : 'No cats in database yet!';
  }

  @Get('search')
  @UsePipes(new JoiValidationPipe(catAgeRangeSchema))
  async getCatsByAgeRange(
    @Query() { age_lte, age_gte }: { age_lte: number; age_gte: number },
  ) {
    const cats = await this.catService.getCatsByAgeRange(age_lte, age_gte);
    return cats.length ? cats : 'No records in the given range!';
  }

  @Get(':id')
  @UsePipes(new JoiValidationPipe(catIdSchema))
  async getCatById(@Param() { id }: { id: number }) {
    const cat = await this.catService.getCatById(id);
    if (!cat)
      throw new HttpException(
        'No record found for specified id',
        HttpStatus.NOT_FOUND,
      );
    return cat;
  }

  @Put(':id')
  async updateCat(
    @Param(new JoiValidationPipe(catIdSchema)) { id }: { id: number },
    @Body(new JoiValidationPipe(updateCatSchema))
    updateCatDto: CreateCatDto,
  ) {
    const catID = await this.catService.updateCat(id, updateCatDto);
    if (!catID)
      throw new HttpException(
        'No record for the specified ID',
        HttpStatus.NOT_FOUND,
      );
    return 'Record updated successfully!';
  }

  @Post()
  @UsePipes(new JoiValidationPipe(createCatSchema))
  async insertCat(@Body() createCatDto: CreateCatDto): Promise<string> {
    return await this.catService.insertCat(
      createCatDto.name,
      createCatDto.age,
      createCatDto.breed,
    );
  }

  @Delete(':id')
  @UsePipes(new JoiValidationPipe(catIdSchema))
  async deleteCat(@Param() { id }: { id: number }) {
    return this.catService.deleteCat(id);
  }
}
