import { Injectable } from '@nestjs/common';
import { CatEntity } from 'src/db/entities/cat.entity';
import { Between } from 'typeorm';

@Injectable()
export class CatService {
  async getCat(): Promise<CatEntity[]> {
    const cats = await CatEntity.find();
    return cats;
  }

  async insertCat(name: string, age: number, breed: string) {
    await CatEntity.insert({
      name,
      age,
      breed,
    });
    return 'Saved record successfully!';
  }

  async getCatById(id: number) {
    const cat = await CatEntity.findOne({
      where: {
        id: id,
      },
    });
    return cat ? cat : null;
  }

  async getCatsByAgeRange(ageL: number, ageH: number) {
    const cats = await CatEntity.find({
      where: [{ age: Between(ageL, ageH) }],
    });

    return cats;
  }

  async updateCat(
    id: number,
    { name, age, breed }: { name?: string; age?: number; breed?: string },
  ) {
    const cat = await this.getCatById(id);
    if (cat) {
      if (name) cat.name = name;
      if (age) cat.age = age;
      if (breed) cat.breed = breed;
      await cat.save();
      return cat.id;
    }
    return null;
  }

  async deleteCat(id: number) {
    await CatEntity.delete({ id });
  }
}
