import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Post,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { ERole } from 'src/db/entities/user.entity';
import { Role } from 'src/decorator/roles.decorator';
import { User } from 'src/decorator/userParam.decorator';
import { AuthGuard } from 'src/guard/auth.guard';
import { RolesGuard } from 'src/guard/role.guard';
import { JoiValidationPipe } from 'src/JoiValidationPipe';
import { createUserSchema } from 'src/schema/userSchema/createUser.schema';
import { loginUserSchema } from 'src/schema/userSchema/loginUser.schema';
import { createUserDto } from './createUserDto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Get('all')
  async getAllUsers() {
    return this.userService.getAllUsers();
  }

  @Post('signup')
  @UsePipes(new JoiValidationPipe(createUserSchema))
  async signup(@Body() createUserDto: createUserDto) {
    const data = await this.userService.createUser(
      createUserDto.name,
      createUserDto.email,
      createUserDto.password,
      createUserDto.role,
    );

    if (data instanceof Error)
      throw new HttpException(data.message, HttpStatus.CONFLICT);
    return data;
  }

  @Post('login')
  @HttpCode(200)
  @UsePipes(new JoiValidationPipe(loginUserSchema))
  async login(
    @Body() { email, password }: { email: string; password: string },
  ) {
    const data = await this.userService.login(email, password);

    if (data instanceof Error)
      throw new HttpException(data.message, HttpStatus.UNAUTHORIZED);

    return data;
  }

  //* Needs user to be authorized
  @Get('profile')
  @UseGuards(AuthGuard)
  async getProfile(@User() { id }: { id: string }) {
    const user = await this.userService.getProfile(id);
    if (user instanceof Error)
      throw new HttpException(user.message, HttpStatus.NOT_FOUND);

    return user;
  }

  //* Needs to be authorized + "ADMIN"
  @Get('admin')
  @Role(ERole.admin)
  @UseGuards(RolesGuard)
  async canBeAccessedByAdmin() {
    return 'You are an ADMIN and hence you can access this route!';
  }

  //* Needs to be authorized + "CUSTOMER"
  @Get('customer')
  @Role(ERole.customer)
  @UseGuards(RolesGuard)
  async canBeAccessedByCustomer() {
    return 'You are a CUSTOMER and hence you can access this route!';
  }
}
